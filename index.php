<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>SAMS 3.0 | Network Sites</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Map showing the spread of government managed internet services sites" />
    <meta name="keywords" content="1GOVNET network sites" />
    <meta name="author" content="MM Arif MZ of Brillante Insights, Malaysia" />

    <!-- Le styles -->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Ubuntu">
    <link type="text/css" rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="assets/leaflet/leaflet.css" />
    <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="assets/leaflet/leaflet.ie.css" /><![endif]-->
    <link type="text/css" rel="stylesheet" href="assets/leaflet/plugins/leaflet.markercluster/MarkerCluster.css" />
    <link type="text/css" rel="stylesheet" href="assets/leaflet/plugins/leaflet.markercluster/MarkerCluster.Default.css" />

    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
      }
      
      html, body, #map {
        position: absolute;
        top: 0;
        bottom: 0;
        width: 100%;
        height :100% !important;
        z-index: 1;
      }

      #loading {
        position: absolute;
        width: 220px;
        height: 19px;
        top: 50%;
        left: 50%;
        margin: -10px 0 0 -110px;
        z-index: 20001;
      }
      #loading .loading-indicator {
        height: auto;
        margin: 0;
      }
      .navbar .brand {
        font-size: 25px;
        font-family: 'Ubuntu', serif;
        font-weight: bold;
        color: white;
      }
      .navbar .nav > li > a {
        padding: 13px 10px 11px;
      }
      .navbar .btn, .navbar .btn-group {
        margin-top: 8px;
      }
      .leaflet-popup-content-wrapper, .leaflet-popup-tip {
        background: #f7f7f7;
      }
      .leaflet-control-geoloc {
        background-image: url(img/location.png);
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
      }

      .title{
        top:100px;
        left:50px;
        right:50px;
        position: absolute;
        overflow: auto;
        z-index: 2;
        text-align: center;
        padding: -5px;
        text-shadow: 1px 1px #f8f8f8;
        color: #FFF;
      }

      .title a{
        color:#1860AA;
        text-decoration: none;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <script type="text/javascript">
      WebFontConfig = {
        google: {
            families: ['Ubuntu::latin']
        }
      };
      (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();
      </script>
    <![endif]-->
  </head>

  <body>

    <div id="map"></div>
    <div id="loading-mask" class="modal-backdrop" style="display:none;"></div>
    <div id="loading" style="display:none;">
        <div class="loading-indicator">
            <img src="img/ajax-loader.gif">
        </div>
    </div>

    <!--div class="title">
      <center>
        <a href="#">
          <img src="img/1-govnet-logo.png" />
        </a>
      </center>
    </div-->

    <!-- Brillante custom javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="assets/bootstrap/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="assets/leaflet/leaflet.js"></script>
    <script type="text/javascript" src="assets/leaflet/plugins/leaflet.markercluster/leaflet.markercluster.js"></script>

    <script type="text/javascript">
      var map, users, mapquest, firstLoad;

      firstLoad = true;

      //users = new L.FeatureGroup();
      users = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: true, 
        showCoverageOnHover: true, 
        zoomToBoundsOnClick: true,
        removeOutsideVisibleBounds: true
      });

     /* mapquest = new L.TileLayer("http://api.smartmapsapi.com/api/a32cdb88-b434-4ba6-ad1d-6999db772260/tile/street/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: '&copy; <a href="http://developer.smartmapsapi.com/">TM Geomatics</a>'*/
      
      mapquest = new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: '&copy; <a href="http://developer.smartmapsapi.com/">TM Smartmap</a> | GITN Sdn Bhd'
      
      });

      map = new L.Map('map', {
        center: new L.LatLng(2.829071252, 101.6028966),
        zoom: 2,
        layers: [mapquest, users]
      });

      // GeoLocation Control
      function geoLocate() {
        map.locate({setView: true, maxZoom: 1});
      }
      var geolocControl = new L.control({
        position: 'topright'
      });
      geolocControl.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'leaflet-control-zoom leaflet-control');
        div.innerHTML = '<a class="leaflet-control-geoloc" href="#" onclick="geoLocate(); return false;" title="My location"></a>';
        return div;
      };
      
      map.addControl(geolocControl);
      map.addControl(new L.Control.Scale());

      //map.locate({setView: true, maxZoom: 3});

      $(document).ready(function() {
        $.ajaxSetup({cache:false});
        $('#map').css('height', ($(window).height() - 40));
        getSites();
      });

      $(window).resize(function () {
        $('#map').css('height', ($(window).height() - 40));
      }).resize();

      function getSites() {
        $.getJSON("get_raw.php", function (data) {
          for (var i = 0; i < data.length; i++) {
            var location = new L.LatLng(data[i].GPS_LAT, data[i].GPS_LONG);

            if(data[i].NET_HEALTH == "UP"){
              var blueDot = L.icon({
                iconUrl: 'img/green-map-marker.png'
              });
            } else if(data[i].NET_HEALTH == "DOWN") {
              var blueDot = L.icon({
                iconUrl: 'img/red-map-marker.png'
              });
            } else {
              var blueDot = L.icon({
                iconUrl: 'img/blue-map-marker.png'
              });
            }
          
              var ministry_agency = "<div style='font-size: 14px; color: #0078A8;'>" + data[i].MINISTRY_AGENCY + "</div>";

              var sitename = "<div style='font-size: 18px;'><b>"+ data[i].SITE_NAME +"</b></div>";

              var site_id = "<div style='font-size: 12px;text-align:left;'>Site ID: <b>"+ data[i].SITE_ID +"</b></div>";

              var net_speed = "<div style='font-size: 12px;text-align:left;'>Network Speed: <b>"+ data[i].NET_SPEED +"</b></div>";

              var net_health = "<div style='font-size: 12px;text-align:left;'>Network Health: <b>"+ data[i].NET_HEALTH +"</b></div>";

               var net_utilin = "<div style='font-size: 12px;text-align:left;'>Utilization In: <b>"+ data[i].NET_UTIL_IN +"%</b></div>";

                var net_utilout = "<div style='font-size: 12px;text-align:left;'>Utilization Out: <b>"+ data[i].NET_UTIL_OUT +"%</b></div>";



            var marker = new L.Marker(location, {
              icon: blueDot
            });
            marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ sitename + ministry_agency + site_id + net_speed + net_health + net_utilin + net_utilout +"</div>", {maxWidth: '400'});
            users.addLayer(marker);
          }
        }).complete(function() {
          if (firstLoad == true) {
            map.fitBounds(users.getBounds());
            firstLoad = false;
          };
        });
      }

    </script>

  </body>
</html>