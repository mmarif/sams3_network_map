<?php

#echo "extraction...";

$db = new PDO('sqlite:leaflet.sqlite');
$sql = "SELECT id, ministry, agency, sitesid, sitesname, lat, lng FROM gitnsites;";

$username = 'aname'; 
$password = 'apassword'; 
$host = 'localhost'; 
$dbname = 'adb';


$rs = $db->query($sql);
if (!$rs) {
    echo "An SQL error occured.\n";
    exit;
}

$rows = array();
while($r = $rs->fetch(PDO::FETCH_ASSOC)) {
    $rows[] = $r;
}
print json_encode($rows);
$db = NULL;
?>